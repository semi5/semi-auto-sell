export function setup(ctx) {
	ctx.onCharacterLoaded(ctx => {
		// debug
		const debugLog = (...msg) => {
			mod.api.SEMI.log(`${id} v10045`, ...msg);
		};

		// crc32
		const makeCRCTable = () => {
			var c;
			var crcTable = [];
			for (var n = 0; n < 256; n++) {
				c = n;
				for (var k = 0; k < 8; k++) {
					c = ((c & 1) ? (0xEDB88320 ^ (c >>> 1)) : (c >>> 1));
				}
				crcTable[n] = c;
			}
			return crcTable;
		};
		const crc32 = function (str, crcTable) {
			var crc = 0 ^ (-1);
			for (var i = 0; i < str.length; i++) {
				crc = (crc >>> 8) ^ crcTable[(crc ^ str.charCodeAt(i)) & 0xFF];
			}
			return (crc ^ (-1)) >>> 0;
		};

		// variables
		const id = "semi-auto-sell";
		const name = "SEMI Auto Sell";
		const icon = ctx.getResourceUrl('semi_icon.png');

		const All_ITEMS = 'All Items'

		const orderMap = new Map(game.bank.defaultSortOrder.map((val, i) => [val, i]));
		const itemList = game.items.allObjects.filter(item => item.type != "" && !item.golbinRaidExclusive);
		const searchItemList = itemList.map((item) => {
			return { item, name: item.name, category: item.category, description: item.description, type: item.type, };
		});

		const crcTable = makeCRCTable();
		const crcFrom = new Map(itemList.map(item => [crc32(item.id, crcTable), item]));
		const crcTo = new Map(itemList.map(item => [item.id, crc32(item.id, crcTable)]));

		const sellMenuItems = new Map();
		const displaySellItems = new Set();

		if (itemList.length != crcFrom.size || itemList.length != crcTo.size) {
			debugLog(`Item Array length doesn't match Map sizes, possible duplicate!`);
		}

		let config = {
			items: [],
			enabled: false,
			protectLockedItems: true,
			minItemCount: 1,
			notification: false,
		};
		let pauseSelling = false;
		let itemLoaded = false;

		// groups
		const sellSortByOrder = (a, b) => (orderMap.get(a) || Infinity) - (orderMap.get(b) || Infinity);

		const rangeDisplay = (item, index) => {
			if (index == 0)
				return All_ITEMS;
			else if (!Number.isFinite(item[1]))
				return `${numberWithCommas(item[0], true)}+`;
			else if (item[0] == item[1])
				return `${numberWithCommas(item[0], true)}`;
			return `${numberWithCommas(item[0], true)} - ${numberWithCommas(item[1], true)}`;
		}

		const GROUP_PRICES = [
			[0, Infinity],
			[0, 0],
			[1, 999],
			[1000, 9999],
			[10000, 99999],
			[100000, 999999],
			[1000000, Infinity]
		];

		const GROUP_TYPES = [All_ITEMS, ...Object.keys(itemList.reduce((acc, curr) => {
			return acc[curr.type] = 1, acc
		}, {})).sort((a, b) => a.localeCompare(b))];
		const GROUP_CATEGORY = [All_ITEMS, ...Object.keys(itemList.reduce((acc, curr) => {
			return acc[curr.category] = 1, acc
		}, {})).sort((a, b) => a.localeCompare(b))];

		const GROUP_SELECT = [All_ITEMS, 'Selected', 'Not Selected'];
		const GROUP_BANK = [All_ITEMS, 'In Bank', 'Not In Bank'];
		const GROUP_LOCKED = [All_ITEMS, 'Locked', 'Not Locked'];

		const filterPrice = (group, item) => {
			let filterValue = group.items[group.displayValue];
			return item.sellsFor.quantity >= filterValue[0] && item.sellsFor.quantity <= filterValue[1];
		};

		const filterKey = (group, item) => {
			let filterValue = group.items[group.displayValue];
			if (filterValue === All_ITEMS) {
				return true;
			}
			return item[group.key] === filterValue;
		};

		const filterSelect = (group, item) => {
			let ret;
			let filterValue = group.items[group.displayValue];

			switch (filterValue) {
				case GROUP_SELECT[0]:
					ret = true;
					break;
				case GROUP_SELECT[1]:
					ret = isItemSelected(item.id);
					break;
				case GROUP_SELECT[2]:
					ret = !isItemSelected(item.id);
					break;
			}
			return ret;
		};

		const filterBank = (group, item) => {
			let ret;
			let filterValue = group.items[group.displayValue];

			switch (filterValue) {
				case GROUP_BANK[0]:
					ret = true;
					break;
				case GROUP_BANK[1]:
					ret = game.bank.hasItem(item);
					break;
				case GROUP_BANK[2]:
					ret = !game.bank.hasItem(item);
					break;
			}
			return ret;
		};

		const filterLocked = (group, item) => {
			let ret;
			let filterValue = group.items[group.displayValue];

			switch (filterValue) {
				case GROUP_LOCKED[0]:
					ret = true;
					break;
				case GROUP_LOCKED[1]:
					ret = game.bank.lockedItems.has(item);
					break;
				case GROUP_LOCKED[2]:
					ret = !game.bank.lockedItems.has(item);
					break;
			}
			return ret;
		};

		const GROUPINGS = [
			{
				name: 'By Price',
				items: GROUP_PRICES,
				render: rangeDisplay,
				filterFun: filterPrice,
				displayValue: 0
			},
			{
				name: 'By Type',
				items: GROUP_TYPES,
				key: 'type',
				filterFun: filterKey,
				displayValue: 0
			},
			{
				name: 'By Category',
				items: GROUP_CATEGORY,
				key: 'category',
				filterFun: filterKey,
				displayValue: 0,
			},
			{
				name: 'By Bank',
				items: GROUP_BANK,
				filterFun: filterBank,
				displayValue: 0
			},
			{
				name: 'By Locked',
				items: GROUP_LOCKED,
				filterFun: filterLocked,
				displayValue: 0
			},
			{
				name: 'By Selected',
				items: GROUP_SELECT,
				filterFun: filterSelect,
				displayValue: 0
			}
		];

		const isItemSelected = (itemid) => config.items.indexOf(itemid) !== -1;
		const isProtectLockedItem = (item) => (config.protectLockedItems && !game.bank.hasUnlockedItem(item));
		const toggleItem = (itemid) => {
			let idx = config.items.indexOf(itemid);
			if (idx !== -1) {
				config.items.splice(idx, 1);
			} else {
				config.items.push(itemid);
			}
		};

		const addItem = (item) => {
			let idx = config.items.indexOf(item.id);
			if (idx == -1) {
				debugLog(`Added ${item.name} to sell list.`);
				config.items.push(item.id);

				const element = sellMenuItems.get(item);
				if (element != undefined) {
					element.classList.add('enable');
				}
			}
		}

		const removeItem = (item) => {
			let idx = config.items.indexOf(item.id);
			if (idx !== -1) {
				debugLog(`Removed ${item.name} from sell list.`);
				config.items.splice(idx, 1);

				const element = sellMenuItems.get(item);
				if (element != undefined) {
					element.classList.remove('enable');
				}
			}
		}

		// modal
		const injectModal = () => {
			// Overlay Modal
			const scriptModal = mod.api.SEMI.buildModal(id, `${name}`);
			scriptModal.blockContainer.html(`
                <div class="block-content pt-0 font-size-sm">
                    <div class="row semi-grid xs pt-1 mb-4">
                        <div class="col-md-5 col-sm-12">
                            <div class="w-100 mb-3">
                                <div class="custom-control custom-switch custom-control-lg">
                                    <input class="custom-control-input" type="checkbox" name="${id}-enable-check" id="${id}-enable-check">
                                    <label class="font-weight-normal ml-2 custom-control-label" for="${id}-enable-check">Enable ${name}</label>
                                </div>
                            </div>
                            <div class="w-100">
                                <div class="custom-control custom-switch custom-control-sm">
                                    <input class="custom-control-input" type="checkbox" name="${id}-protect-items-enable-check" id="${id}-protect-items-enable-check">
                                    <label class="font-weight-normal ml-2 custom-control-label" for="${id}-protect-items-enable-check">Ignore Locked Bank Items</label>
                                </div>
                            </div>
                            <div class="w-100">
                                <div class="custom-control custom-switch custom-control-sm">
                                    <input class="custom-control-input" type="checkbox" name="${id}-show-notifications-enable-check" id="${id}-show-notifications-enable-check">
                                    <label class="font-weight-normal ml-2 custom-control-label" for="${id}-show-notifications-enable-check">Show Sell Notifications</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="w-100 pl-1">Minimum quantity to keep:</div>
                            <div class="w-50"><input type="number" min="0" class="form-control m-1" name="${id}-min-item-keep" id="${id}-min-item-keep" placeholder="1"></div>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="w-100 semi-grid-item mb-1">
                                Disabled
                            </div>
                            <div class="w-100 semi-grid-item enable">
                                Enabled
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-content pt-0 font-size-sm" id="${id}-item-groups">
                    <h2 class="content-heading border-bottom mb-2 pb-2">Filter By Groups</h2>
                    <button role="button" id="${id}-reset-filter-btn" class="btn btn-sm btn-danger mr-2">RESET</lang-string>
                    </button>
                </div>
                <div class="block-content pt-0 font-size-sm">
                    <div class="pb-4">
                        <h2 class="content-heading border-bottom mb-2 pb-1 row no-gutters">
                            <div class="mt-2 flex-grow-1">Item Toggles <small id="${id}-filter-text" class="text-info"></small></div>
                            <div class="col-12 col-md-4">
                                <div class="form-group col-12 mb-0">
                                    <div class="input-group">
                                        <input type="text" class="form-control text-danger" id="${id}-searchTextbox-items" name="${id}-searchTextbox-items" placeholder="Search Item...">
                                        <div class="input-group-append">
                                        <button type="button" id="${id}-search-filter-btn" class="btn btn-danger";">X</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </h2>
                        <div class="row semi-grid sm" id="${id}-container"></div>
                    </div>
                </div>
            `);

			$(scriptModal.modal).on('hidden.bs.modal', () => {
				pauseSelling = false;
				storeConfig();

				if (config.enabled) {
					itemList.forEach(item => {
						processItem(item);
					});
				}
			});

			$(`#${id}-enable-check`).on('change', function (e) {
				toggleEnabledStatus();
			}).prop('checked', config.enabled);

			$(`#${id}-protect-items-enable-check`).on('change', function (e) {
				toggleSellLockItemStatus();
			}).prop('checked', config.protectLockedItems);

			$(`#${id}-show-notifications-enable-check`).on('change', function (e) {
				toggleNotificationStatus();
			}).prop('checked', config.notification);

			$(`#${id}-min-item-keep`).bind('keyup change input', function (e) {
				let newMin = parseInt($(this).val(), 10);
				if (isNaN(newMin) || newMin < 0) newMin = 0;
				config.minItemCount = newMin;
			}).val(config.minItemCount);

			$(`#${id}-reset-filter-btn`).on('click', function (e) {
				resetAllFilter();
			});

			$(`#${id}-search-filter-btn`).on('click', function (e) {
				clearSellItemSearch();
			});

			// group building
			let itemGroupsContainer = $(`#${id}-item-groups`);

			const buildDropdownElement = (group, group_index) => {
				const options = createElement('div', {
					className: 'dropdown-menu dropdown-menu-right overflow-y-auto font-size-sm',
					attributes: [
						['aria-labeledby', `${id}-group-dropdown-${group_index}`],
						['style', 'max-height: 60vh; z-index: 9999;']
					]
				});

				group.items.forEach((item, selection_index) => {
					let dropBtn = createElement('button', {
						className: 'dropdown-item',
						attributes: [['type', 'button']],
						children: (group.render ? group.render(item, selection_index) : item)
					});
					dropBtn.onclick = () => changeGroupDisplay(group_index, selection_index);
					options.append(dropBtn);
				});

				const main = createElement('div', {
					classList: ['dropdown', 'd-inline'],
					children: [
						createElement('button', {
							id: `${id}-group-dropdown-${group_index}`,
							className: ['btn btn-outline-info dropdown-toggle mr-2'],
							attributes: [
								['type', 'button'],
								['data-toggle', 'dropdown'],
								['aria-haspopup', 'true'],
								['aria-expanded', 'false']
							],
							children: [group.name]
						}),
						options
					]
				});

				return main;
			};

			GROUPINGS.forEach((group, index) => {
				itemGroupsContainer.append(buildDropdownElement(group, index));
			});

			// Bank Shortcuts
			$('bank-item-settings-menu').find('.row-deck').append(`<div class="col-12 pl-1 pt-3"><h5 class="font-w700 text-left text-combat-smoke m-1">${name}</h5></div>
				<div class="dropdown-divider w-100"></div>
				<div class="col-12 p-1">
					<div class="btn-group mb-2 w-100" role="group">
						<div class="col-6 pl-0"><button role="button" class="btn btn-sm btn-success w-100" id="${id}-shortcut-add">Add to Auto Sell</button></div>
						<div class="col-6 pr-0"><button role="button" class="btn btn-sm btn-danger w-100" id="${id}-shortcut-remove">Remove from Auto Sell</button></div>
					</div>
				</div>`);

			$(`#${id}-shortcut-add`).on('click', function (e) {
				if (game.bank.selectedBankItem != undefined) {
					addItem(game.bank.selectedBankItem.item);
					storeConfig();
				}
			});
			$(`#${id}-shortcut-remove`).on('click', function (e) {
				if (game.bank.selectedBankItem != undefined) {
					removeItem(game.bank.selectedBankItem.item);
					storeConfig();
				}
			});
		};

		const showModal = () => {
			pauseSelling = true;
			if (!itemLoaded)
				buildItems();
			updateModal();
			$(`#modal-${id}`).modal('show');
		};

		const updateModal = () => {
			updateItems();
			updateSearchTextBox();
		};

		const toggleEnabledStatus = () => {
			config.enabled = !config.enabled;
		};

		const toggleSellLockItemStatus = () => {
			config.protectLockedItems = !config.protectLockedItems;
		};

		const toggleNotificationStatus = () => {
			config.notification = !config.notification;
		};

		const resetAllFilter = () => {
			GROUPINGS.forEach((group) => {
				group.displayValue = 0;
			});
			$(`#${id}-searchTextbox-items`).val('');
			updateModal();
		};

		const updateSearchTextBox = () => {
			if (displaySellItems.size === 0) {
				$(`#${id}-searchTextbox-items`).addClass('text-danger');
			} else {
				$(`#${id}-searchTextbox-items`).removeClass('text-danger');
			}
		}

		const clearSellItemSearch = () => {
			$(`#${id}-searchTextbox-items`).val('');
			updateModal();
		};

		const changeGroupDisplay = (group, selection) => {
			GROUPINGS[group].displayValue = selection;
			updateModal();
		};

		const shouldShow = (item => {
			for (let group of GROUPINGS) {
				if (!group.filterFun(group, item)) {
					return false;
				}
			}
			return true;
		});

		const updateDisplaySellItems = () => {
			let result;
			let query = $(`#${id}-searchTextbox-items`).val();
			if (query === '') {
				result = searchItemList;
			} else {
				const options = {
					shouldSort: true,
					tokenize: true,
					matchAllTokens: true,
					findAllMatches: true,
					threshold: 0.1,
					location: 0,
					distance: 100,
					maxPatternLength: 32,
					minMatchCharLength: 1,
					keys: ['name', 'category', 'type', 'description'],
				};

				const fuse = new Fuse(searchItemList, options);
				result = fuse.search(query);
			}

			displaySellItems.clear();
			result.forEach((result) => {
				if (shouldShow(result.item)) {
					displaySellItems.add(result.item);
				}
			});
		}

		const updateItems = () => {
			const filterContainer = $(`#${id}-filter-text`);
			filterContainer.empty();
			GROUPINGS.forEach((group) => {
				let filterValue = group.render ? group.render(group.items[group.displayValue], group.displayValue) : group.items[group.displayValue];
				if (filterValue !== All_ITEMS) {
					const filterNode = $(`<span class="badge badge-primary ml-1 pointer-enabled">${filterValue}</span>`);
					filterNode.on('click', () => {
						group.displayValue = 0;
						updateModal();
					});
					filterContainer.append(filterNode);
				}
			});

			updateDisplaySellItems();

			itemList.forEach((item) => {
				const element = sellMenuItems.get(item);
				if (element === undefined) {
					return;
				}
				if (displaySellItems.has(item)) {
					showElement(element);
				} else {
					hideElement(element);
				}
			});
		}

		const buildItems = () => {
			if (!itemLoaded) {
				const container = document.getElementById(id + '-container');
				$(container).html(`<div class="col-12 text-center"><span class="spinner-border text-info skill-icon-md"></span></div>`);
				window.setTimeout(() => {
					container.textContent = '';
					itemList.sort(sellSortByOrder).forEach((item) => {
						const sellItem = createElement('div', {
							className: "semi-grid-item float m-1 " + (isItemSelected(item.id) ? 'enable' : ''),
							attributes: [['data-item', item.id], ['data-tippy-content', `${item.name} - ${item.sellsFor.currency.formatAmount(numberWithCommas(item.sellsFor.quantity))}`]],
							parent: container,
							children: [createElement('img', {
								attributes: [['src', item.media], ['alt', item.name]],
							})]
						})

						container.append(sellItem);
						sellMenuItems.set(item, sellItem);
					});
					$(`#${id}-searchTextbox-items`).keyup(function () {
						updateModal();
					});

					tippy(`#${id}-container [data-tippy-content]`, {
						animation: false,
						allowHTML: true
					});

					$(`#${id}-container .semi-grid-item`).on('click', function (e) {
						const elm = $(this);
						const itemid = elm.data("item");

						elm.toggleClass('enable');
						toggleItem(itemid);
					});

				}, 1000);
				itemLoaded = true;
			}
		}

		// script
		const processItem = (item) => {
			if (!config.enabled || !isItemSelected(item.id) || isProtectLockedItem(item) || pauseSelling) {
				return;
			}

			const qty = game.bank.getQty(item) - config.minItemCount;

			// Exit if there's nothing to sell.
			if (qty <= 0) {
				return;
			}

			// Sell the items
			game.bank.processItemSale(item, qty);

			// Send a notification to the user
			debugLog(`Selling ${numberWithCommas(qty)} x ${item.name}`);

			if (config.notification)
				game.notifications.createInfoNotification(`semi-auto-sell-${item.id}`, `Selling ${numberWithCommas(qty)} x ${item.name}`, icon, 0);
		}

		// config
		/**
		 * Write a compressed string from the config item array.
		 * Compress about 1500 items into the 8kb limit.
		 */
		const itemWriter = (inputItems) => {
			let writer = new SaveWriter('Write', 128);

			writer.writeUint32(inputItems.length);
			inputItems.forEach(item => {
				writer.writeUint32(crcTo.get(item));
			});

			const rawSaveData = writer.getRawData();
			const compressedData = fflate.strFromU8(fflate.zlibSync(new Uint8Array(rawSaveData)), true);
			const saveString = btoa(compressedData);
			return saveString;
		}

		/**
		 * Decodes a compress item array containing only item ids.
		 * Input at max would be around 900 items at the 8kb mod save limit.
		 */
		const itemReader_v1 = (saveString) => {
			const reader = new SaveWriter('Read', 1);
			try {
				reader.setRawData(fflate.unzlibSync(fflate.strToU8(atob(saveString), true)).buffer);
			} catch (_a) {
				return null;
			}

			// Decode Namespace
			let namespaces = [];
			let namespaceMap = {};
			let namespaceCount = reader.getInt32();
			for (let i = 0; i < namespaceCount; i++) {
				let item = reader.getString();
				namespaces[i] = item;
				namespaceMap[item] = i;
			}

			// Decode Items
			let items = [];
			let itemCount = reader.getInt32();
			for (let i = 0; i < itemCount; i++) {
				let ns = reader.getUint8();
				let item = reader.getString();
				items[items.length] = `${namespaces[ns]}:${item}`;
			}
			return items;
		};

		/**
		 * Decodes a compress item array containing only the crc32 of item ids.
		 * Input at max would be around 1500 items at the 8kb mod save limit.
		 */
		const itemReader_v2 = (saveString) => {
			const reader = new SaveWriter('Read', 1);
			try {
				reader.setRawData(fflate.unzlibSync(fflate.strToU8(atob(saveString), true)).buffer);
			} catch (_a) {
				return null;
			}

			// Decode Items
			let items = [];
			let itemCount = reader.getUint32();
			for (var i = 0; i < itemCount; i++) {
				const itemCRC = reader.getUint32();
				const item = crcFrom.get(itemCRC);
				if (item) {
					items[items.length] = item.id;
				} else {
					debugLog(`Decoded CRC had no matching item: 0x${itemCRC.toString(16)}`);
				}
			}
			return items;
		};

		const storeConfig = () => {
			const builtConfig = {
				enabled: config.enabled,
				protectLockedItems: config.protectLockedItems,
				notification: config.notification,
				minItemCount: config.minItemCount,
				crcItems: itemWriter(config.items)
			};
			ctx.characterStorage.setItem('config', builtConfig);
		}

		const loadConfig = () => {
			const storedConfig = ctx.characterStorage.getItem('config');
			if (!storedConfig) {
				return;
			}

			if (storedConfig.enabled == true)
				config.enabled = true;

			if (storedConfig.protectLockedItems == false)
				config.protectLockedItems = false;

			if (storedConfig.notification == true)
				config.notification = true;

			if (storedConfig.minItemCount != null)
				config.minItemCount = storedConfig.minItemCount;

			// Read Stored Items
			if (storedConfig.items != null) { // v1
				debugLog('Reading v1 items');
				const namespaceItems = itemReader_v1(storedConfig.items);
				if (namespaceItems) {
					config.items = namespaceItems;
				}
			} else if (storedConfig.crcItems != null) { // v2
				debugLog('Reading v2 items');
				const crcItems = itemReader_v2(storedConfig.crcItems);
				if (crcItems) {
					config.items = crcItems;
				}
			}
		}

		// hooks + game patches
		loadConfig();

		// First do a pass on the players bank, processing all valid items
		if (config.enabled) {
			itemList.forEach(item => {
				processItem(item);
			});
		}

		// Patches the add item to bank function, running this every time an object we care about is added to the bank
		ctx.patch(Bank, 'addItem').after((didAddItem, item, ...args) => {
			if (didAddItem) {
				processItem(item);
			}
		});

		ctx.onInterfaceReady(() => {
			injectModal();
			mod.api.SEMI.addSideBarModSetting(name, icon, showModal);
		});
	});
}
